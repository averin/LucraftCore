package lucraft.mods.lucraftcore.util.abilitybar;

public enum AbilityBarPos {

    RIGHT,
    LEFT,
    DISABLED;

}
